# Frontex

Frontex is a Minecraft plugin written by 14mRh4X0r for MinecraftOnline on the Sponge plugin platform. It provides BanService functionality together with Notes, a feature designed to help MinecraftOnline's staff team to function.

## Installation

The prerequisite to using Frontex is having Sponge installed on your server. Sponge's documentation (https://docs.spongepowered.org/stable/en/server/index.html) has the instructions that. Build Frontex's artifact file and put it into your ``mods`` folder of the server. It will automatically generate the ``frontex`` folder with the ``frontext.conf`` file inside of your server ``config`` folder.

You need to set up a MySQL database and then put its JDBC URL into the ``frontex.conf``, otherwise Frontex won't do anything.

## Note Types

- Info notes: staff members can see them using ``/note list <username>``.
- Warning notes: staff members and the warned user see them in chat when the warned user logs in.
- Ban notes: banned users see them when they attempt to log in, can be seen using ``/banwhy <username>``.
- Log notes: automatically created when a note was either removed or demoted.

## Commands

Pipes in commands mean "or".

- **/note** ...
    - Central command for adding, demoting and removing notes
    - **list** [**username**]
        - Lists the notes of the given user, otherwise lists your own notes
        - Users without the ``frontex.note.list`` permission can only use it to see their own warnings notes
    - **warn** <**username**> <**message**>
        - Adds a warning note with the given message onto the user
        - Staff members are informed of the new warning note
    - **info** <**username**> <**message**>
        - Adds an info note with the given message onto the user
        - Staff members are informed of the new info note
    - **remove | rem | delete | del** <**username**> <**note-id**>
        - Removes the note with the given ID from the given user
        - Automatically creates a log note informing that the note was removed
    - **demote** <**username**> <**note-id**>
        - Demotes the note with the given ID of a given user
        - Ban notes are demoted into warning notes
        - Warning notes are demoted into info notes
        - Automatically creates a log note informing that the note was demoted

- **/banwhy | isplayerbanned** <**username**>
    - Gives the ban reason for the given user if they are banned

- **/ban** <**username**> [**reason**]
    - Bans the given user
    - Automatically creates a ban note with the given reason, otherwise uses ``"Banned by [STAFF MEMBER] at [CURRENT TIME]"`` as the ban reason

- **/unban** <**username**>
    - Unbans the given user
    - Demotes all ban notes that belong to the user to warning notes

## Argument Notation

| Notation | Meaning |
| :------: | :-----: |
| ``<argument>`` | Required |
| ``[argument]`` | Optional |

## Permissions

| Permission | Meaning |
| :--------: | :-----: |
| ``frontex.ban`` | Allows the user to do ``/ban`` |
| ``frontex.unban`` | Allows the user to do ``/unban`` |
| ``frontex.reason`` | Allows the user to do ``/banwhy`` |
| ``frontex.note.list`` | Allows the user to do ``/note list`` |
| ``frontex.note.warn`` | Allows the user to do ``/note warn`` |
| ``frontex.note.info`` | Allows the user to do ``/note info`` |
| ``frontex.note.remove`` | Allows the user to do ``/note remove`` |
| ``frontex.note.demote`` | Allows the user to do ``/note demote`` |

## Licensing

This project is licensed under the GNU Affero GPL license. You should have a copy of it together with this project. If not, see http://www.gnu.org/licenses/.

## Credits

14mRh4x0r (Willem Mulder) - Creator

Vagankovo - Contributor