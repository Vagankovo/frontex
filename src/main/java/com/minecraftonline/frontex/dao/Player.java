/*
 * Copyright (C) 2017 Willem Mulder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.frontex.dao;

import be.bendem.sqlstreams.Update;
import com.minecraftonline.frontex.Frontex;
import com.minecraftonline.frontex.sql.UpdateReturning;
import lombok.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

/**
 * DAO for players.
 * @author Willem Mulder
 */
@Getter
@AllArgsConstructor
@ToString
@EqualsAndHashCode(of = "uuid")
public class Player {
    /**
     * The database ID of this object.
     * A value of -1 means that this player is not yet in the database.
     */
    private int id = -1;

    /**
     * The {@link UUID} of this player.
     */
    private UUID uuid;

    /**
     * The last known name of this player.
     */
    @Setter
    private String name;

    /**
     * The first time this player logged on to the server.
     */
    private Instant firstLogin;

    /**
     * The last time this player logged on to the server.
     * <strong>NOTE:</strong> this is not the last time this player logged out!
     */
    @Setter
    private Instant lastLogin;

    public Player(@NonNull UUID uuid, @NonNull String name, @NonNull Instant firstLogin, @NonNull Instant lastLogin) {
        this.uuid = uuid;
        this.name = name;
        this.firstLogin = firstLogin;
        this.lastLogin = lastLogin;
    }

    /**
     * Saves local changes to the database.
     * @return {@code true} if the associated database record was updated or inserted, {@code false} otherwise.
     */
    public boolean save() {
        if (id == -1) {
            try (UpdateReturning update = new UpdateReturning(Frontex.getInstance().getSql(),
                            "INSERT INTO players (uuid, name, first_login, last_login) VALUES (?, ?, ?, ?)")
                    .with(uuid, name, firstLogin, lastLogin)) {
                update.execute();
                Optional<Integer> newId = update.generated(rs -> rs.getInt(1)).findFirst();

                if (newId.isPresent()) {
                    id = newId.get();
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            try (Update update = Frontex.getInstance().getSql()
                    .update("UPDATE players SET uuid = ?, name = ? , first_login = ?, last_login = ? WHERE id = ?")
                    .with(uuid, name, firstLogin, lastLogin, id)) {
                return update.count() > 0;
            }
        }
    }

    /**
     * Find a player by its database ID.
     * @param id the database ID of the player.
     * @return the player, if found.
     */
    public static Optional<Player> find(int id) {
        return Frontex.getInstance().getSql()
                .first("SELECT * FROM players WHERE id = ?", Player::fromResultSet, id);
    }

    /**
     * Find a player by its profile UUID.
     * @param uuid the profile {@link UUID} of the player.
     * @return the player, if found.
     */
    public static Optional<Player> findByUUID(@NonNull UUID uuid) {
        return Frontex.getInstance().getSql()
                .first("SELECT * FROM players WHERE uuid = ?", Player::fromResultSet, uuid.toString());
    }

    protected static Player fromResultSet(ResultSet rs) throws SQLException {
        return new Player(
                rs.getInt("id"),
                UUID.fromString(rs.getString("uuid")),
                rs.getString("name"),
                Instant.ofEpochMilli(rs.getTimestamp("first_login").getTime()),
                Instant.ofEpochMilli(rs.getTimestamp("last_login").getTime())
        );
    }

    static {
        Frontex.getInstance().getSql()
                .registerCustomBinding(Instant.class, (stmt, idx, instant) -> stmt.setTimestamp(idx, Timestamp.from(instant)))
                .registerCustomBinding(UUID.class, (stmt, idx, uuid) -> stmt.setString(idx, uuid.toString()))
                .registerCustomBinding(Player.class, (stmt, idx, player) -> stmt.setInt(idx, player.getId()));
    }
}
