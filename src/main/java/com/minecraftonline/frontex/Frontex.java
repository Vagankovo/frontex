/*
 * Copyright (C) 2017 Willem Mulder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.frontex;

import be.bendem.sqlstreams.SqlStream;
import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;
import javax.sql.DataSource;
import lombok.Data;
import lombok.Getter;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.Setting;
import ninja.leaping.configurate.objectmapping.serialize.ConfigSerializable;
import org.flywaydb.core.Flyway;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.config.ConfigDir;
import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.GameReloadEvent;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.event.game.state.GameStoppedEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.service.sql.SqlService;

/**
 *
 * @author Willem Mulder
 */
@Plugin(id = "frontex")
public class Frontex {
    @Inject
    @Getter
    private Logger logger;

    @Inject
    @ConfigDir(sharedRoot = false)
    private Path configDir;

    @Inject
    @DefaultConfig(sharedRoot = false)
    private ConfigurationLoader<CommentedConfigurationNode> configManager;

    @Inject
    @DefaultConfig(sharedRoot = false)
    private Path configFile;

    @Getter
    private static Frontex instance;

    @Getter
    private SqlStream sql;
    private Settings settings;

    public Frontex() {
        instance = this;
    }

    @Listener
    public void onInit(GameInitializationEvent evt) {
        try {
            Files.createDirectories(configDir);
            this.loadSettings();

            if (settings.jdbcUrl.isEmpty()) {
                logger.error("No database configured. Frontex won't do anything.");
                return;
            }

            this.configureDatabase();
        } catch (IOException | ObjectMappingException e) {
            throw new RuntimeException("Could not load settings", e);
        } catch (SQLException e) {
            throw new RuntimeException("Failed to get DataSource", e);
        }

        Sponge.getServiceManager().setProvider(this, org.spongepowered.api.service.ban.BanService.class, new BanService());

        Sponge.getCommandManager().register(this, Commands.BAN, "ban");
        Sponge.getCommandManager().register(this, Commands.UNBAN, "unban");
        Sponge.getCommandManager().register(this, Commands.NOTE, "note");
        Sponge.getCommandManager().register(this, Commands.REASON, "reason", "isplayerbanned", "banwhy");

        Sponge.getEventManager().registerListeners(this, new PlayerListener());
    }

    @Listener
    public void onReload(GameReloadEvent evt) {
        try {
            this.loadSettings();

            if (settings.jdbcUrl.isEmpty()) {
                logger.error("No database configured. Frontex won't do anything.");
                return;
            }

            this.configureDatabase();
        } catch (IOException | ObjectMappingException e) {
            throw new RuntimeException("Could not load settings", e);
        } catch (SQLException e) {
            throw new RuntimeException("Failed to get DataSource", e);
        }
    }

    private void configureDatabase() throws SQLException {
        DataSource dataSource = Sponge.getServiceManager()
                .provideUnchecked(SqlService.class)
                .getDataSource(this, settings.jdbcUrl);

        logger.info("Migrating database...");
        Flyway.configure().dataSource(dataSource).load().migrate();
        // Does not actually open a connection
        sql = SqlStream.connect(dataSource);
    }

    @Listener
    public void onStopped(GameStoppedEvent evt) {
        if (sql != null) {
            sql.close();
        }
    }

    private void loadSettings() throws IOException, ObjectMappingException {
        if (Files.notExists(configFile)) {
            this.configManager.save(this.configManager.createEmptyNode().setValue(Settings.TYPE, new Settings()));
            logger.info("Created default config");
        }

        settings = configManager.load().getValue(Settings.TYPE);
    }

    @ConfigSerializable
    @Data
    public static final class Settings {
        @SuppressWarnings("UnstableApiUsage")
        public static final TypeToken<Settings> TYPE = TypeToken.of(Settings.class);

        @Setting(value = "database",
                 comment = "The alias or JDBC URL of the database, of the form jdbc:driver:details")
        private String jdbcUrl;
    }
}
